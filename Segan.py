import time
import wave
import random
import subprocess
import RPi.GPIO   as     GPIO
from   rpi_ws281x import PixelStrip, Color

LED_COUNT      = 92          # Number of LED pixels.
LED_PIN        = 18          # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000      # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10          # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255         # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False       # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0           # set to '1' for GPIOs 13, 19, 41, 45 or 53

LED_SUN_START  = 0
LED_SUN_END    = 3
LED_MERCURY    = 6
LED_MARS       = 12
LED_EARTH      = 16
LED_MARS       = 26
LED_JUPYTER    = 91        

strip = PixelStrip(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

random.seed()

GPIO_PIN_SUN      = 2
GPIO_PIN_START    = 3
GPIO_PIN_SHUTDOWN = 4 

GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_PIN_SUN,      GPIO.IN)
GPIO.setup(GPIO_PIN_START,    GPIO.IN)
GPIO.setup(GPIO_PIN_SHUTDOWN, GPIO.IN)

TIME_TO_TRAVEL_TO_MOON    = 1
TIME_TO_TRAVEL_TO_MERCURY = 316
TIME_TO_TRAVEL_TO_VENUS   = 138
TIME_TO_TRAVEL_TO_MARS    = 260 
TIME_TO_TRAVEL_TO_JUPYTER = 2098
TIME_TO_TRAVEL_TO_SATURN  = 4281
TIME_TO_TRAVEL_TO_URANUS  = 9098
TIME_TO_TRAVEL_TO_NEPTUN  = 14521
TIME_TO_TRAVEL_TO_PLUTO   = 19095

LED_PER_SECOND = (LED_JUPYTER - LED_EARTH) / TIME_TO_TRAVEL_TO_JUPYTER

signal_start_time = 0
planet_output     = 0

def animateSun(state):
    if not state:
        r = 226
        g = 121
        b = 35

        for i in range(LED_SUN_START, LED_SUN_END):
            rd = random.randint(0, 55)
            r -= rd
            g -= rd
            b -= rd

            if r < 0:  
               r = 0
            
            if g < 0:  
               g = 0

            if b < 0:  
               b = 0
            
            color = Color(r, g, b)
            strip.setPixelColor(i, color)
    else:
        color = Color(0,0,0)
        for i in range(LED_SUN_START, LED_SUN_END):
           strip.setPixelColor(i, color)

def animateRadio():
    
  for i in range(LED_COUNT):
    strip.setPixelColor(i, Color(0,0,0))

  if signal_start_time > 0:
      sec = int((time.time() - signal_start_time))
      led_left  = LED_EARTH - int(sec * LED_PER_SECOND)
      led_right = LED_EARTH + int(sec * LED_PER_SECOND)
      
      if led_left > 0:
          strip.setPixelColor(led_left, Color(0, 0, 250))

      if led_right < LED_COUNT:
          strip.setPixelColor(led_right, Color(0, 0, 250))

def playSound():
    global planet_output

    if signal_start_time == 0:
        return

    if planet_output == 0:
        if (time.time() - signal_start_time) > TIME_TO_TRAVEL_TO_MOON:
           subprocess.call(['aplay','s.wav'])
           planet_output += 1

    if planet_output == 1:
        if (time.time() - signal_start_time) > TIME_TO_TRAVEL_TO_VENUS:
           subprocess.call(['aplay','s.wav'])
           planet_output += 1

    if planet_output == 2:
        if (time.time() - signal_start_time) > TIME_TO_TRAVEL_TO_MERCURY:
           subprocess.call(['aplay','s.wav'])
           planet_output += 1

    if planet_output == 3:
        if (time.time() - signal_start_time) > TIME_TO_TRAVEL_TO_MARS:
           subprocess.call(['aplay','s.wav'])
           planet_output += 1

    if planet_output == 4:
        if (time.time() - signal_start_time) > TIME_TO_TRAVEL_TO_JUPYTER:
           subprocess.call(['aplay','s.wav'])
           planet_output += 1

    if planet_output == 5:
        if (time.time() - signal_start_time) > TIME_TO_TRAVEL_TO_SATURN:
           subprocess.call(['aplay','s.wav'])
           planet_output += 1

    if planet_output == 6:
        if (time.time() - signal_start_time) > TIME_TO_TRAVEL_TO_URANUS:
           subprocess.call(['aplay','s.wav'])
           planet_output += 1

    if planet_output == 7:
        if (time.time() - signal_start_time) > TIME_TO_TRAVEL_TO_NEPTUN:
           subprocess.call(['aplay','s.wav'])
           planet_output += 1

    if planet_output == 8:
        if (time.time() - signal_start_time) > TIME_TO_TRAVEL_TO_PLUTO:
           subprocess.call(['aplay','s.wav'])
           planet_output += 1

while True:

    if not GPIO.input(GPIO_PIN_START):
        signal_start_time = time.time()
        planet_output = 0

    if GPIO.input(GPIO_PIN_SHUTDOWN):
        subprocess.call(["/sbin/halt", "-p"])

    playSound()
    animateRadio()
    animateSun(GPIO.input(GPIO_PIN_SUN))
    strip.show()
    time.sleep(0.23)
